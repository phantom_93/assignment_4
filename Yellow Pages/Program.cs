﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata;

namespace Yellow_Pages
{
    class Program
    {
        static IList<string> stringList;

        static void Main(string[] args)
        {
            // registry of stringList 
            stringList = new List<string>() {
                "Anna Banana" ,
                "Breaking Bad" ,
                "Anna Potato",
                "John Doe" ,
                "Jane Doe"
            };
            Console.WriteLine("Registered contacts are:");
            foreach (var item in stringList)
            {
                Console.WriteLine(item);
            }
            UserInput();

        }

        static void UserInput()
        {
            
            bool choice = true;
            while (choice)
            {
                Console.WriteLine("enter 'p' to continue and 'e' to exit");
                Console.WriteLine("Enter a character/characters for partial matches. (p)");
                Console.WriteLine("Exit (e)");
                string menu = Console.ReadLine();
                switch (menu)
                {

                    case "p":
                        NameMatch(stringList);
                        break;
                    case "e":
                    default:
                        choice = false;
                        break;
                }
            }


        }
        // this function checks if the given name by user is in the registry or not 
        public static void NameMatch(IList<string> stringList)
        {

            Console.WriteLine("Enter a character");
            string userCharacter = Console.ReadLine().ToLower();
            bool found = false;
            //  
            foreach (var str in stringList)
            {

                if (str.ToLower().Contains(userCharacter))
                {
                    Console.WriteLine($"contact found: ({str})");
                    found = true;
                }

            }
            // if contacts are not found in the registry
            if (found != true)
            {
                Console.WriteLine($"contact not found");

            }

        } // end of PartialMatch/ fullmatch function

    }
}

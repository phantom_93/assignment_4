﻿using System;

namespace Draw_Square
{
    class Program
    {
        static void Main(string[] args)
        {
            bool restart = true;

            while (restart)
            {
                Console.Clear();
                int height = UserInput("Enter the size of the height you want to draw");
                Console.WriteLine("");
                int width = UserInput("Enter the size of the width you want to draw");
                Console.WriteLine("");
                // the function to draw the height and width that user enttered 
                DrawSquareRectangle(height, width);

                restart = RestartQuery();
            }
            // exit the program immediately 
            Environment.Exit(0);
        }

        // the following function does ask the user if he/she wants to continue or not. 
        static bool RestartQuery()
        {
            repeat:
            bool restart = true;

            Console.Write("Would you like to restart (yes=y / no=n): ");
            string response = Console.ReadLine();

            switch(response)
            {
                case "yes":
                    break;
                case "y":
                    break;
                case "no":
                    restart = false;
                    break;
                case "n":
                    restart = false;
                    break;
                default:
                    goto repeat;
            }

            return restart;
        }

        // function to take the inputs from user    
        public static int UserInput(string userChoice) 
        {

            bool isValid = false;
            int size = 5;

            while (!isValid)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write($"{userChoice}: ");

                try
                {

                    size = Convert.ToInt32(Console.ReadLine());
                    isValid = true;
                    // if the user entered negative number, it wil give error 
                    if (size <= 0)
                    {
                        isValid = false;
                        throw new ArgumentOutOfRangeException();
                    }
                }
                catch (Exception ex)
                { // if the user entered a string, it will give error 
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine($"You have entered the invalid input. (Error: {ex.Message})");
                }
            }

            return size;
        }

        // this fucntion draw either squares or rectangls 
        public static void DrawSquareRectangle(int height, int width)
        {
            // if you entered same height and width,  you have entered square, if not rectangle 
            Console.Write("you have entered: ");
            if (width == height)
            {
                Console.WriteLine("Square\n");
            }
            else
            {
                Console.WriteLine("Rectangle\n");
            }
            for (int i = 0; i < height; i++) // to draw the rows of the square/ rectangle 
            {
                Console.Write("*"); // to draw the first character 
                for (int j = 1; j < (width - 1); j++) // to draw the columns of the square/rectangle
                {
                    if (i == 0 || i == (height - 1)) // if the row is between 0 and the last number - 1, then fill in the specific character, else leave it 
                    {
                        Console.Write(" *"); // to draw the middle characters
                    }
                    else
                    {
                        Console.Write("  "); // for spacing 
                    }
                }
                Console.WriteLine(" *"); // to draw th last character row 
            }
        }





    }
}
